import express from 'express';
import cookieParser from 'cookie-parser';
import {getHomePage, getLoginPage} from './controllers/viewsController.js'
import authMiddleware from './middlewares/authMiddleware.js';
import { login } from './controllers/authController.js';

const app = express()

app.set('view engine', 'ejs');


app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use('/static', express.static('assets'));
app.use(authMiddleware);


app.get('/', getHomePage);
app.get('/login', getLoginPage);
app.post('/login', login);

export default app;