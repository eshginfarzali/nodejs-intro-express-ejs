import fetch from 'node-fetch';

export async function getHomePage(req, res) {
  try {
    const response = await fetch('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=30&page=1&sparkline=false');

    if (!response.ok) {
      throw new Error('API request failed');
    }

    const data = await response.json();

    if (!Array.isArray(data)) {
      throw new Error('API response is not an array');
    }

    res.render('home', {
      data: data,
    });
  } catch (error) {
    console.error('Error:', error);
    res.status(500).send('An error occurred.');
  }
}

export function getLoginPage(req, res) {
  res.render('login');
}
