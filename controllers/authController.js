import crypto from 'crypto';

export function login(req, res) {
  const { username, password } = req.body;
  if (username === 'jhondoe' && password === '12345') {
    const access_token = crypto.randomBytes(64).toString('base64');
    res.cookie('access_token', access_token, {
      expires: new Date(Date.now() + 900000),
    });
    res.redirect('/');
  } else {
    res.status(401).send('Username or password is incorrect!');
  }
}
