export default function authMiddleware(req, res, next) {
    if (req.url === '/login') {
      next();
      return;
    }
  
    if (req.cookies.access_token) {
      next();
    } else {
      res.redirect('/login');
    }
  }
  